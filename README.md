# Cinema DRF API

API service for cinema management written on DRF.

## Run locally
Install PostgreSQL and create db
```shell
git clone git@gitlab.com:lotrikys/dockerize-drf-cinema.git
cd dockerize-drf-cinema
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
set POSTGRES_HOST=<your db ip or hostname>
set POSTGRES_DB=<your database name>
set POSTGRES_USER=<your database username>
set POSTGRES_PASSWORD=<your database password>
python manage.py migrate
python manage.py runserver
```
To create superuser
```shell
python manage.py createsuperuser
```
## Run with docker-compose
.env example
```text
POSTGRES_HOST=db
POSTGRES_DB=cinema
POSTGRES_USER=cinema_user
POSTGRES_PASSWORD=cinema_password
```
```shell
mv .env.sample .env
docker-compose build
docker-compose up -d
```
## Features
* JWT authenticated
* Admin panel /admin/
* Documentation /api/doc/swagger/ or /api/doc/redoc/
* Managing orders and tickets
* Creating movies with actors and genres
* Adding picture for movie /api/movie/{movie_id}/upload-image/
* Creating cinema halls
* Adding movie sessions
* Filtering movies and movie sessions
